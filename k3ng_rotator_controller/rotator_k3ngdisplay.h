
#ifndef _K3NGDISPLAY_H
#define _K3NGDISPLAY_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

/*


                  Set the display type here !!!!!!!!!

*/



#define FEATURE_4_BIT_LCD_DISPLAY
// #define FEATURE_ADAFRUIT_I2C_LCD
// #define FEATURE_YOURDUINO_I2C_LCD
// #define FEATURE_RFROBOT_I2C_DISPLAY
// #define FEATURE_YWROBOT_I2C_DISPLAY
// #define FEATURE_SAINSMART_I2C_LCD
// #define FEATURE_ADAFRUIT_BUTTONS                

// #define OPTION_RFROBOT_I2C_DISPLAY_BACKLIGHT_OFF

#include "rotator_hardware.h"

#ifdef HARDWARE_EA4TX_ARS_USB
  #include "rotator_features_ea4tx_ars_usb.h"
#endif
#ifdef HARDWARE_WB6KCN
  #include "rotator_features_wb6kcn.h"
#endif
#ifdef HARDWARE_M0UPU
  #include "rotator_features_m0upu.h"
#endif
#ifdef HARDWARE_TEST
  #include "rotator_features_test.h"
#endif    
#if !defined(HARDWARE_CUSTOM)
  #include "rotator_features.h" 
#endif   

#ifdef HARDWARE_EA4TX_ARS_USB
  #include "rotator_pins_ea4tx_ars_usb.h"
#endif
#ifdef HARDWARE_M0UPU
  #include "rotator_pins_m0upu.h"
#endif
#ifdef HARDWARE_WB6KCN
  #include "rotator_pins_wb6kcn.h"
#endif
#ifdef HARDWARE_TEST
  #include "rotator_pins_test.h"
#endif
#if !defined(HARDWARE_CUSTOM)
  #include "rotator_pins.h"
#endif

#if defined(FEATURE_ADAFRUIT_I2C_LCD)
  #include "rotator.h"
#endif

#define K3NG_DISPLAY_LIBRARY_VERSION "2018.03.08.01"
#define MAX_SCREEN_BUFFER_COLUMNS 20
#define MAX_SCREEN_BUFFER_ROWS 4

#define ATTRIBUTE_BLINK B00000001

#define TEXT_BLINK_MS 500
#define WORK_STRING_SIZE 32

#define I2C_LCD_COLOR WHITE            // default color of I2C LCD display, including Adafruit and Yourduino; some Yourduino units may want this as LED_ON                  
// #define I2C_LCD_COLOR GREEN                  
// #define I2C_LCD_COLOR LED_ON
#include "LiquidCrystal.h"

class K3NGdisplay {

public:

    K3NGdisplay(int display_columns, int display_rows, int _update_time);
    void initialize();
    void service(uint8_t force_update_flag);  // write pending changes to the screen periodically and blink text that has the blink attribute
    void clear();   // clear the display immediately
    void clear_pending_buffer();
    void update();  // update pending changes to the screen
    void print(char * print_string);
    void print(char * print_string,int x,int y);
    void print(char * print_string,int x,int y, uint8_t text_attribute);
    void print_attribute(char * print_string, uint8_t text_attribute);
    void print_attribute(char * print_string,int x,int y, uint8_t text_attribute);
    void print_center(char * print_string,int y);
    void print_center_padded(char * print_string,int y,int padding);
    void print_center_fixed_field_size(char * print_string,int y,int field_size);
    void print_center_entire_row(char * print_string,int y,uint8_t text_attribute);
    void print_center(char * print_string,int y,uint8_t text_attribute);
    void print_center_screen(char * print_string);
    void print_center_screen(char * print_string,uint8_t text_attribute);
    void print_center_screen(char * print_string,char * print_string2);
    void print_center_screen(char * print_string,char * print_string2,char * print_string3);
    void print_center_screen(char * print_string,char * print_string2,char * print_string3,char * print_string4);
    void print_right(char * print_string,int y);
    void print_right_padded(char * print_string,int y,int padding);
    void print_right_fixed_field_size(char * print_string,int y,int field_size);
    void print_left(char * print_string,int y);
    void print_left_padded(char * print_string,int y,int padding);
    void print_left_fixed_field_size(char * print_string,int y,int field_size);
    void print_top_left(char * print_string);
    void print_top_right(char * print_string);
    void print_bottom_left(char * print_string);
    void print_bottom_right(char * print_string); 

    /* print a timed message in the center of the screen; this can be multiline */

    void print_center_timed_message(char * print_string,int ms_to_display);  
    void print_center_timed_message(char * print_string,int ms_to_display,uint8_t text_attribute);  // TODO - add multiline timed attribute prints
    void print_center_timed_message(char * print_string,char * print_string2,int ms_to_display);
    void print_center_timed_message(char * print_string,char * print_string2,char * print_string3,int ms_to_display);
    void print_center_timed_message(char * print_string,char * print_string2,char * print_string3,char * print_string4,int ms_to_display);

    void redraw();  // redraw the entire screen
    void row_scroll();
    void println(char * print_string);
    int length(char * print_string);
    void clear_row(uint8_t row_number);  // clear one entire row
    void userDefinedChars(void);         // agregado por lu9aga para grabar en el LCD los User defined Characters
   
    #if defined(FEATURE_ADAFRUIT_BUTTONS)
      uint8_t readButtons();
    #endif   

private:

	void save_current_screen_to_revert_screen_buffer();  // used by the timed message functionality to push the current screen to a buffer for saving
	//void push_revert_screen_buffer_to_live_buffer();  // used by the timed message functionality to pull a saved screen and push to the live display
    void push_revert_screen_buffer_to_pending_buffer();
	void revert_back_screen();                           // used by the timed message functionality to pull a saved screen and push to the live display
    void prepare_for_timed_screen(int ms_to_display);
    int Xposition(int screen_buffer_index);
    int Yposition(int screen_buffer_index);
    int buffer_index_position(int x,int y);
    void defChar (LiquidCrystal &thelcd, int cgram, const unsigned char row[8]); // agregado por lu9aga para grabar un User defined Character
};

/* lu9aga
// Los siguientes arrays definen o dibujan caracteres definidos por  nosotros.
// Los LCD basados en Hitachi 44780 y similares aceptan que agreguemos hasta 8 
// caracteres especiales definidos por el usuario. Deben grabarse en la memoria 
// CGRAM del LCD y se pierden al des energizar el display
// Ideal si no encontramos en el Set de Caracteres de nuestro LCD algun simbolo
// en epecial o si queremos abreviar por ejemplo, la secuencia "°c", que ocupa
// 2 posiciones en un unico simbolo equivalente
// En C o C++ u otros lenguajes, solo podremos usar 7 de estas posiciones ya que
// para imprimir uno de estos simbolos deberemos enviar al LCD los valores 0x00
// a 0x07. Pero el valor 0x00 equivale a un NULL y casualmente es el valor que
// se usa para indicar un fin de string, por lo que el simbolo que se grabe en
// la posicon 0x00 de la CGram no se puede imprimir.
*/

static unsigned char degree[8] = {                        
   0x08, 0x14, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00
   };                                                     // simbolo grados
   
static unsigned char arrowLeft[8] = {                        
   0x01, 0x3, 0x7, 0xf, 0x7, 0x3, 0x1, 0x00
   };                                                     // simbolo flecha a Izq.

static unsigned char arrowRight[8] = {                        
   0x10, 0x18, 0x1c, 0x1e, 0x1c, 0x18, 0x10, 0x00
   };                                                     // simbolo flecha a Der.

static unsigned char arrowDown[8] = {                        
   0x00, 0x0, 0x0, 0x00, 0x1f, 0xe, 0x04, 0x00
   };                                                     // simbolo flecha Abajo

static unsigned char arrowUp[8] = {                        
   0x4, 0xe, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00
   };                                                     // simbolo flecha Arriba


static unsigned char degreeCelcius[8] = {                 
   0x08, 0x14, 0x08, 0x03, 0x04, 0x04, 0x03, 0x00
   };                                                     // simbolo grados Celcius


static unsigned char degreeMag[8] = {                 
   0x08, 0x14, 0x08, 0x03, 0x04, 0x04, 0x03, 0x00
   };                                                     // simbolo grados Magneticos


static unsigned char degreeTrue[8] = {                 
   0x08, 0x14, 0x08, 0x03, 0x04, 0x04, 0x03, 0x00
   };                                                     // simbolo grados True

static unsigned char milibars[8] = {                 
   0x0A, 0x15, 0x11, 0x00, 0x04, 0x06, 0x05, 0x06
   };                                                     // simbolo milibares

static unsigned char velocidadNudos[8] = {                 
   0x14, 0x18, 0x14, 0x00, 0x07, 0x02, 0x02, 0x00
   };                                                     // simbolo Millas Nauticas por hora o Nudos

static unsigned char velocidadKMH[8] = {                 
   0x14, 0x18, 0x14, 0x00, 0x05, 0x07, 0x05, 0x00
   };                                                     // simbolo kilometros por hora      
                 
 // Se pueden agregar cuantos simbolos se necesiten pero solo hay
 // 7 bytes -utilizables en C++- en la memoria en el LCD para almacenarlos.
 // Para grabar algun simbolo en particular en alguna de las 7 memorias
 // hemos agregado un metodo publico a la clase  K3NGdisplay, userDefinedChars()
 // y un metodo privado, void defChar (LiquidCrystal &thelcd, int asc, const unsigned char row[8]).
 // Este ultimo es el que grabara cada uno de los "user defined characters" en el LCD y deberemos
 // proporcionarle el nombre del objeto, la posicion de CGRAM (1-7) donde queremos almacenar el simbolo
 // y por supuesto la definicion del simbolo en cuestión.                                                            
 // lu9aga


#endif //_K3NGDISPLAY_H
